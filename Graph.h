#include<fstream>
#include<iostream>
#include<vector>
#include<set>
#include <sstream>
#include <string>
#include <algorithm>
#include<queue>

//система непересекающихся множеств
class DSU
{
private:

	int N;
	int *p;
	int *rank;

public:

	//не учитываем 0
	DSU(int n = 100)
	{
		N = n;
		p = new int[N+1];
		rank = new int[N+1];
		cleaning();
	}

	~DSU()
	{
		delete p;
		delete rank;
	}

	void cleaning()
	{
		for (int i = 0; i <= N; i++)
		{
			rank[i] = 0;
			p[i] = i;
		}
	}

	//найти вершину
	int find(int x)
	{
		return (x == p[x] ? x : p[x] = find(p[x]));
	}

	//соединить
	void unite(int x, int y)
	{
		if ((x = find(x)) == (y = find(y)))
			return;

		if (rank[x] <  rank[y])
			p[x] = y;
		else
			p[y] = x;

		if (rank[x] == rank[y])
			++rank[x];
	}

	//состаляют ли все вершины одно дерево?
	bool isOneTree()
	{
		int cur = find(1);
		for (int i = 2; i <= N; i++)
		{
			if (cur != find(i))
				return false;
		}
		return true;
	}

	//вектор с номерами веришин всех деревьев (слишком долго)
	std::vector<int> getTops()
	{
		std::vector<int> result(0);
		int empty = 0;
		for (int i = 1; i <= N; i++)
		{
			bool check = false;
			empty = find(i);
			for (int j = 0; j < result.size(); j++)
			{
				if (result[j] == empty)
				{
					check = true;
					break;
				}
			}
			result.push_back(empty);
		}
		return result;
	}

	//возвращает все индексы принадлежащие дереву с вершиной top
	std::vector<int> getTreeIndexes(int top)
	{
		std::vector<int> result(0);
		for (int i = 1; i <= N; i++)
		{
			if (find(i) == top)
				result.push_back(i);
		}
		return result;
	}
};

//виртуальный класс
class BaseGraphPresentation
{
protected:
	struct VectorInt3 {
		int first;
		int second;
		int third;
	};

public:

	int N; //вершины
	int M; //рёбра
	bool D; //ориентированность
	bool W; //взвешенность
	char IND;
	virtual void readGraph(std::string fileName) = 0;
	virtual void addEdge(int from, int to, int weight) = 0;
	virtual void removeEdge(int from, int to) = 0;
	virtual int changeEdge(int from, int to, int newWeight) = 0; 
	virtual void writeGraph(std::string fileName) = 0;

};

//матрица смежности
class AdjMatrixGraph : public BaseGraphPresentation
{
private:
	friend class ListOfEdgesGraph;
	friend class AdjListGraph;

	std::vector< std::vector<int>> arr;

	void CreateArray(int n)
	{
		n++;
		arr.resize(n);
		for (int i = 0; i < n; i++)
			arr[i].resize(n);
	}

	bool dfs(std::vector<int> &Rarr,std::vector<int> &Larr, std::vector<bool> &mark,
		std::vector<int> &rm, std::vector<int> &lm, int u)
	{
		//bool result;
		for(int v = 0; v < Rarr.size();v++)
		{
			if(arr[Rarr[v]][Larr[u]] != 0 && !mark[v])
			{
				mark[v] = true;
				if(rm[v] == -1 || dfs(Rarr, Larr, mark,rm,lm, rm[v]))
				{
					lm[u] = v;
					rm[v] = u;
					return true;
				}
			}
		}
		return false;
	}

public:

	AdjMatrixGraph(AdjMatrixGraph& a);/*
									  {
									  N = a.N;
									  M = a.M;
									  IND = 'C';
									  D = a.D;
									  W = a.W;
									  arr.resize(N);
									  for(int i = 0; i < N; i++)
									  {
									  arr[i].resize(N);
									  for(int j = 0; j < N; j++)
									  {
									  arr[i][j] = a.arr[i][j];
									  }
									  }
									  }*/

	AdjMatrixGraph(ListOfEdgesGraph& a);/*
										{
										IND = 'C';
										N = a.N;
										M = a.M;
										D = a.D;
										W = a.W;
										CreateArray(N);
										for(int i = 0; i < M; i++)
										{
										if(W)
										arr[a.arr[i].first][a.arr[i].second] = a.arr[i].third;
										else
										arr[a.arr[i].first][a.arr[i].second] = 1;
										if(!D)
										arr[a.arr[i].second][a.arr[i].first] = arr[a.arr[i].first][a.arr[i].second];
										}
										}*/

	AdjMatrixGraph(AdjListGraph& a);/*
									{
									IND = 'C';
									N = a.N;
									M = a.M;
									D = a.D;
									W = a.W;
									CreateArray(N);
									for(int i = 0; i < N; i++)
									{
									for(int j = 0; j < a.arr[i].size();i++)
									{
									if(W)
									arr[i][a.arr[i][j].first] = a.arr[i][j].second;
									else
									arr[i][a.arr[i][j].first] = 1;
									if(!D)
									arr[a.arr[i][j].first][i] = arr[i][a.arr[i][j].first];
									}
									}
									}*/

	AdjMatrixGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'C';
	}

	AdjMatrixGraph(int n, bool d, bool w)
	{
		N = n;
		M = 0;
		D = d;
		W = w;
		CreateArray(n);
		IND = 'C';
	}

	void readGraph(std::string fileName)
	{
		char ch;
		std::ifstream fin(fileName, std::ios_base::in);

		M = 0;
		fin >> ch >> N >> D >> W;
		CreateArray(N);
		for (int i = 1; i <= N; i++)
		{
			for (int j = 1; j <= N; j++)
			{
				fin >> arr[i][j];
				if (arr[i][j] != 0)
					M++;
			}
		}
		if (!D) M /= 2;

		fin.close();
	}

	int changeEdge(int from, int to, int newWeight)
	{
		int oldWeight = arr[from][to];
		arr[from][to] = newWeight;
		if (!D)
			arr[to][from] = newWeight;
		return oldWeight;
	}

	void addEdge(int from, int to, int weight = 1)
	{
		M++;
		changeEdge(from, to, weight);
	}

	void removeEdge(int from, int to)
	{
		M--;
		changeEdge(from, to, 0);
	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc);

		fout << IND << " " << N << std::endl
			<< D << " " << W << std::endl;
		for (int i = 1; i <= N; i++)
		{
			fout << arr[i][1];
			for (int j = 2; j <= N; j++)
				fout << " " << arr[i][j];
			fout << std::endl;
		}

		fout.close();
	}

	AdjMatrixGraph* getSpaingTreePrima()
	{
		std::vector<bool> IV(N + 1, false);
		int k = 0;
		AdjMatrixGraph* result = new AdjMatrixGraph(N, D, W);
		IV[1] = true;
		k++;
		while (k<N)
		{
			int z = -1;
			int z1 = 0;
			int min = std::numeric_limits<int>::max();
			//поиск не включённых
			for (int i = 1; i <= N; i++)
			{		
				if (!IV[i])
				{
					//поиск включённых связынным с не включённым 
					for (int j = 1; j <= N; j++)
					{
						//есть ли ребро к включённому
						if (arr[i][j] != 0 && IV[j] && arr[i][j] < min)
						{
							z = i;
							z1 = j;
							min = arr[i][j];
						}
					}
				}
			}
			if (z != -1)
			{
				IV[z] = true;
				result->addEdge(z, z1, min);
				k++;
			}
			else
				break;//что-то пошло не так
		}
		return result;
	}

	int checkBipart(std::vector<char> &marks)
	{
		std::queue<int> q;
		//по вершинам, которые ещё не взяты
		for (int i = 1; i <= N; i++)
		{
			if (marks[i] != 'A' && marks[i] != 'B')
			{
				marks[i] = 'A';
				q.push(i);
				int k;
				//bfs от вершины которую только что взяли
				while (!q.empty())
				{
					k = q.front();
					q.pop();
					for (int j = 1; j <= N; j++)
					{
						if(arr[k][j] != 0)
						{
							//если вершина новая
							if (marks[j] != 'A' && marks[j] != 'B')
							{
								if(marks[k] == 'B')
									marks[j] = 'A';
								else
									marks[j] = 'B';
								q.push(j);
							}
							//если нет чередования
							if(marks[j] == marks[k])
								return 0;
						}
					}
				}
			}
		}
		return 1;
	}

	std::vector<std::pair<int,int> > getMaximumMatchingBipart()
	{
		std::vector<char> marks(N+1, 'E');
		std::vector<int> Rarr(0), Larr(0);
		checkBipart(marks);
		for(int i = 1; i <= N; i++)
		{
			if(marks[i] == 'A')
				Larr.push_back(i);
			if(marks[i] == 'B')
				Rarr.push_back(i);
		}

		if(Larr.size() > Rarr.size())
			std::swap(Larr,Rarr);
		std::vector<int> rm(Rarr.size(),-1), lm(Larr.size(),-1);

		//предалгоритм
		for(int i = 0; i < lm.size(); i++)
		{
			for(int j = 0; j < rm.size(); j++)
			{
				if(lm[i] == -1 && rm[j] == -1 && arr[Larr[i]][Rarr[j]] != 0)
				{
					lm[i] = j;
					rm[j] = i;
				}
			}
		}
		


		//сам алгоритм
		std::vector<bool> mark(rm.size(), false);
		for(int i = 0; i < lm.size(); i++)
		{
			if(lm[i] == -1)
			{
				mark.assign(rm.size(), false);
				dfs(Rarr, Larr, mark,rm,lm, i);
			}
		}

		std::vector<std::pair<int,int> > result;
		std::pair<int,int> p(0,0);
		for(int i = 0; i < lm.size(); i++)
		{
			if(lm[i] != -1)
			{
				p.first = Larr[i];
				p.second = Rarr[lm[i]];
				result.push_back(p);
			}
		}
		return result;
	}
};

//список смежности
class AdjListGraph : public BaseGraphPresentation
{//( i )---->( j )
private:
	friend class AdjMatrixGraph;
	friend class ListOfEdgesGraph;
	std::vector<std::vector< std::pair<int, int> > > arr;

	void CreateArray(int n)
	{
		n++;
		arr.resize(n);
		for (int i = 0; i < n; i++)
			arr[i].resize(0);
	}

	int searchJ(int i, int first)
	{
		for (int j = 0; j < arr[i].size(); j++)
		{
			if (arr[i][j].first == first)
			{
				return j;
			}
		}
		return -1;
	}

	static bool cmp(const std::pair<int, int> &a, const std::pair<int, int> &b)
	{
		return a.second < b.second;
	}

	//сортировка по возрастанию
	static void SortArr(std::vector< std::pair<int, int> > &AR)
	{
		//sort(AR.begin(), AR.begin() + AR.size(), cmp);
		/*for (int i = 1; i < AR.size(); i++)
		{
			bool changes = false;
			for (int j = 0; j < AR.size() - i; j++)
			{
				if (AR[j].second > AR[j + 1].second)
				{
					std::swap(AR[j], AR[j + 1]);
					changes = true;
				}
			}
			if (!changes)
				break;
		}*/
	}

	

public:

	AdjListGraph(AdjMatrixGraph& a);/*
									{
									std::pair<int, int> p(0,1);
									IND = 'L';
									N = a.N;
									M = a.M;
									D = a.D;
									W = a.W;
									CreateArray(N);
									for(int i = 0; i < N; i++)
									{
									for(int j = 0; j < N; j++)
									{
									if(a.arr[i][j] != 0)
									{
									p.first = j;
									p.second = a.arr[i][j];
									arr[i].push_back(p);
									}
									}
									}
									}*/

	AdjListGraph(ListOfEdgesGraph& a);/*
									  {
									  std::pair<int, int> p;
									  IND = 'L';
									  N = a.N;
									  M = a.M;
									  D = a.D;
									  W = a.W;
									  CreateArray(N);
									  for(int i = 0; i < M; i++)
									  {
									  p.first = a.arr[i].second;
									  p.second = a.arr[i].third;
									  arr[a.arr[i].first].push_back(p);
									  }
									  }*/

	AdjListGraph(AdjListGraph& a);/*
								  {
								  IND = 'L';
								  N = a.N;
								  M = a.M;
								  D = a.D;
								  W = a.W;
								  CreateArray(N);
								  for(int i = 0; i < N; i++)
								  {
								  arr[i].resize(a.arr[i].size());
								  for(int j = 0; j < a.arr[i].size(); j++)
								  {
								  arr[i][j] = a.arr[i][j];
								  }
								  }
								  }*/

	AdjListGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'L';
	}

	AdjListGraph(int n, bool d, bool w)
	{
		N = n;
		M = 0;
		D = d;
		W = w;
		IND = 'L';
		CreateArray(n);
	}

	void readGraph(std::string fileName)
	{
		char ch;
		int k;
		std::pair<int, int> p(0, 1);
		std::ifstream fin(fileName, std::ios_base::in);
		std::string line;

		M = 0;
		fin >> ch >> N >> D >> W;
		std::getline(fin, line);
		CreateArray(N);
		for (int i = 1; i <= N; i++)
		{
			//fin >> k;
			std::getline(fin, line);
			std::istringstream stream(line);
			while (stream >> p.first)
			{
				if (W)
					stream >> p.second;
				arr[i].push_back(p);
				M++;
			}
		}

		fin.close();
	}

	int changeEdge(int from, int to, int newWeight)
	{
		int oldWeight = -1;

		int j = searchJ(to, from);
		if (j != -1)
		{
			oldWeight = arr[to][j].second;
			arr[to][j].second = newWeight;
		}
		if (!D && j == -1)
		{
			j = searchJ(from, to);
			if (j != -1)
			{
				oldWeight = arr[from][j].second;
				arr[from][j].second = newWeight;
			}
		}

		return oldWeight;
	}

	void addEdge(int from, int to, int weight = 1)
	{
		std::pair<int, int> a(to, weight);
		M++;
		arr[from].push_back(a);
	}

	void removeEdge(int from, int to)
	{
		int j = searchJ(to, from);
		if (j != -1)
			arr[to].erase(arr[to].begin() + j);
		if (!D && j == -1)
		{
			j = searchJ(from, to);
			arr[from].erase(arr[from].begin() + j);
		}
		M--;

	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc);

		fout << IND << " " << N << std::endl
			<< D << " " << W << std::endl;
		for (int i = 1; i <= N; i++)
		{
			if (!arr[i].empty())
			{
				fout << arr[i][0].first;
				if (W)
					fout << " " << arr[i][0].second;

				for (int j = 1; j < arr[i].size(); j++)
				{
					fout << " " << arr[i][j].first;
					if (W)
						fout << " " << arr[i][j].second;
				}
			}
			fout << std::endl;
		}

		fout.close();
	}

	AdjListGraph* getSpaingTreePrima()
	{
		//сортировка по возрастанию
		for (int i = 1; i <= N; i++)
			sort(arr[i].begin(), arr[i].begin() + arr[i].size(), cmp);
			//SortArr(arr[i]);

		std::vector<bool> IV(N + 1, false);
		int k = 0;
		AdjListGraph* result = new AdjListGraph(N, D, W);
		IV[1] = true;
		k++;
		while (k<N)
		{
			int z = -1;
			int z1 = 0;
			int min = std::numeric_limits<int>::max();
			//проход по всем вершинам
			for (int i = 1; i <= N; i++)
			{
				//проход по всем соединённым вершинам с i-той
				for (int j = 0; j < arr[i].size(); j++)
				{
					//если вес ещё не привышает найденного минимума
					if (arr[i][j].second < min)
					{
						//есть ли ребро к включённым
						if (IV[arr[i][j].first] != IV[i])
						{
							z = i;
							z1 = arr[i][j].first;
							min = arr[i][j].second;
						}
					}
					else
						break;
				}
				
			}
			//если всё ок, до добавлем ребро
			if (z != -1)
			{
				if(!z)
					IV[z] = true;
				else
					IV[z1] = true;
				result->addEdge(z, z1, min);
				k++;
			}
			else
				break;//что-то пошло не так
		}
		return result;
	}

	

};

//список рёбер
class ListOfEdgesGraph : public BaseGraphPresentation
{
private:

	friend class AdjMatrixGraph;
	friend class AdjListGraph;

	std::vector< VectorInt3 > arr;

	int SearchEdge(int from, int to)
	{
		for (int i = 0; i < M; i++)
		{
			if ((arr[i].first == from && arr[i].second == to) ||
				(!D && arr[i].first == to && arr[i].second == from))
			{
				return i;
			}
		}
		return -1;
	}

	static bool cmp(const VectorInt3 &a, const VectorInt3 &b)
	{
		return a.third < b.third;
	}

public:

	ListOfEdgesGraph(AdjMatrixGraph& a);

	ListOfEdgesGraph(ListOfEdgesGraph& a);

	ListOfEdgesGraph(AdjListGraph& a);

	ListOfEdgesGraph()
	{
		N = 0;
		M = 0;
		D = false;
		W = false;
		IND = 'E';
		arr.resize(0);
	}

	ListOfEdgesGraph(int n, bool d, bool w)
	{
		N = n;
		M = 0;
		D = d;
		W = w;
		IND = 'E';
		arr.resize(0);
	}

	void readGraph(std::string fileName)
	{
		char ch;
		VectorInt3 v;
		std::ifstream fin(fileName, std::ios_base::in);

		v.third = 1;
		M = 0;
		fin >> ch >> N >> M >> D >> W;
		for (int i = 0; i < M; i++)
		{
			fin >> v.first >> v.second;
			if (W)
				fin >> v.third;
			arr.push_back(v);
		}

		fin.close();
	}

	int changeEdge(int from, int to, int newWeight)
	{
		int i = SearchEdge(from, to);
		int oldWeight = arr[i].third;
		arr[i].third = newWeight;
		return oldWeight;
	}

	void addEdge(int from, int to, int weight = 1)
	{
		M++;
		VectorInt3 v;
		v.first = from;
		v.second = to;
		v.third = weight;
		arr.push_back(v);
	}

	void removeEdge(int from, int to)
	{
		M--;
		int i = SearchEdge(from, to);
		arr.erase(arr.begin() + i);
	}

	void writeGraph(std::string fileName)
	{
		std::ofstream fout(fileName, std::ios_base::out | std::ios_base::trunc);

		fout << IND << " " << N << " " << M << std::endl
			<< D << " " << W << std::endl;
		for (int i = 0; i < M; i++)
		{
			fout << arr[i].first << " " << arr[i].second;
			if (W)
				fout << " " << arr[i].third;
			fout << std::endl;
		}

		fout.close();
	}

	ListOfEdgesGraph* getSpaingTreeKruscal()
	{
		sort(arr.begin(), arr.begin() + arr.size(), cmp);
		DSU IV(N);
		ListOfEdgesGraph* result = new ListOfEdgesGraph(N,D,W);
		for (int i = 0; i < M; i++)
		{
			if (IV.find(arr[i].first) != IV.find(arr[i].second))
			{
				result->addEdge(arr[i].first, arr[i].second, arr[i].third);
				IV.unite(arr[i].first, arr[i].second);
				if (IV.isOneTree())
					break;
			}
		}
		return result;
	}

	ListOfEdgesGraph* getSpaingTreeBoruvka()
	{
		ListOfEdgesGraph* result = new ListOfEdgesGraph(N, D, W);
		std::vector< VectorInt3 > E(N + 1);
		int K = M;
		DSU comp(N);
		VectorInt3 fr;
		fr.first = 0;
		fr.second = 0;
		fr.third = std::numeric_limits<int>::max();
		for (int i = 0; i <= N; i++)
		{
			E[i] = fr;
		}
		bool changes = true;
		while (changes)
		{
			changes = false;
			//проход по рёбрам
			for (int i = 0; i < K; i++)
			{
				int fcomp = comp.find(arr[i].first);
				int scomp = comp.find(arr[i].second);
				if (fcomp == scomp)
				{
					K--;
					std::swap(arr[i], arr[K]);
					i--;
				}
				else
				{
					if (E[fcomp].third > arr[i].third)
						E[fcomp] = arr[i];
					if (E[scomp].third > arr[i].third)
						E[scomp] = arr[i];
				}
			}
			//проход по массиву с компонентами
			for (int i = 0; i < N; i++)
			{
				if (E[i].third != std::numeric_limits<int>::max())
				{
					if (comp.find(E[i].first) != comp.find(E[i].second))
					{
						changes = true;
						result->addEdge(E[i].first, E[i].second, E[i].third);
						comp.unite(E[i].first, E[i].second);
					}
					E[i].third = std::numeric_limits<int>::max();
				}
			}
		}
		return result;
	}

};

//переоределение конструкторов
AdjMatrixGraph::AdjMatrixGraph(AdjMatrixGraph& a)
{
	N = a.N;
	M = a.M;
	IND = 'C';
	D = a.D;
	W = a.W;
	CreateArray(N);
	for (int i = 1; i <= N; i++)
	{
		for (int j = 1; j <= N; j++)
		{
			arr[i][j] = a.arr[i][j];
		}
	}
}

AdjMatrixGraph::AdjMatrixGraph(ListOfEdgesGraph& a)
{
	IND = 'C';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	CreateArray(N);
	for (int i = 0; i < M; i++)
	{
		if (W)
			arr[a.arr[i].first][a.arr[i].second] = a.arr[i].third;
		else
			arr[a.arr[i].first][a.arr[i].second] = 1;
		if (!D)
			arr[a.arr[i].second][a.arr[i].first] = arr[a.arr[i].first][a.arr[i].second];
	}
}

AdjMatrixGraph::AdjMatrixGraph(AdjListGraph& a)
{
	IND = 'C';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	CreateArray(N);
	for (int i = 1; i <= N; i++)
	{
		for (int j = 0; j < a.arr[i].size(); j++)
		{
			if (W)
				arr[i][a.arr[i][j].first] = a.arr[i][j].second;
			else
				arr[i][a.arr[i][j].first] = 1;
			if (!D)
				arr[a.arr[i][j].first][i] = arr[i][a.arr[i][j].first];
		}
	}
}


AdjListGraph::AdjListGraph(AdjMatrixGraph& a)
{
	std::pair<int, int> p(0, 1);
	IND = 'L';
	N = a.N;
	M = 0;
	D = a.D;
	W = a.W;
	CreateArray(N);
	int k = 0;
	if (!D)
		k++;
	for (int i = 1; i <= N; i++)
	{
		for (int j = i*k + (1 - k); j <= N; j++)
		{
			if (a.arr[i][j] != 0)
			{
				addEdge(i,j,a.arr[i][j]);
			}
		}
	}
}

AdjListGraph::AdjListGraph(ListOfEdgesGraph& a)
{
	std::pair<int, int> p;
	IND = 'L';
	N = a.N;
	M = 0;
	D = a.D;
	W = a.W;
	CreateArray(N);
	for (int i = 0; i < a.M; i++)
	{
		addEdge(a.arr[i].first, a.arr[i].second,a.arr[i].third); 
	}

}

AdjListGraph::AdjListGraph(AdjListGraph& a)
{
	IND = 'L';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	CreateArray(N);
	for (int i = 1; i <= N; i++)
	{
		arr[i].resize(a.arr[i].size());
		for (int j = 0; j < a.arr[i].size(); j++)
		{
			arr[i][j] = a.arr[i][j];
		}
	}
}


ListOfEdgesGraph::ListOfEdgesGraph(AdjMatrixGraph& a)
{
	IND = 'E';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	arr.resize(0);
	VectorInt3 p;
	p.third = 1;
	int k = 0;
	if (!D)
		k++;
	for (int i = 1; i <= N; i++)
	{
		for (int j = i*k + (1 - k); j <= N; j++)
		{
			if (a.arr[i][j] != 0)
			{
				p.first = i;
				p.second = j;
				if (W)
					p.third = a.arr[i][j];
				arr.push_back(p);
			}
		}
	}

}

ListOfEdgesGraph::ListOfEdgesGraph(ListOfEdgesGraph& a)
{
	IND = 'E';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	arr.resize(M);
	for (int i = 0; i < M; i++)
	{
		arr[i] = a.arr[i];
	}
}

ListOfEdgesGraph::ListOfEdgesGraph(AdjListGraph& a)
{
	IND = 'E';
	N = a.N;
	M = a.M;
	D = a.D;
	W = a.W;
	arr.resize(0);
	VectorInt3 p;
	p.third = 1;
	for (int i = 1; i <= N; i++)
	{
		p.first = i;
		for (int j = 0; j < a.arr[i].size(); j++)
		{
			p.second = a.arr[i][j].first;
			if (W)
				p.third = a.arr[i][j].second;
			arr.push_back(p);
		}
	}
}

//гравный класс
class Graph
{
private:

	BaseGraphPresentation* graph;
	AdjMatrixGraph* AMGraph;
	AdjListGraph* ALGraph;
	ListOfEdgesGraph* LEGraph;

	void CreateObjectGraph(char ind, int n = 0, int m = 0, bool d = false, bool w = false)
	{
		delete graph;

		if (ind == 'C')
		{
			AMGraph = new AdjMatrixGraph(n, d, w);
			ALGraph = 0;
			LEGraph = 0;
			graph = AMGraph;
		}
		if (ind == 'L')
		{
			AMGraph = 0;
			ALGraph = new AdjListGraph(n, d, w);
			LEGraph = 0;
			graph = ALGraph;
		}
		if (ind == 'E')
		{
			AMGraph = 0;
			ALGraph = 0;
			LEGraph = new ListOfEdgesGraph(n, d, w);
			graph = LEGraph;
		}
	}

public:

	Graph()
	{
		graph = new AdjMatrixGraph();
		CreateObjectGraph('C');
	}

	Graph(AdjMatrixGraph* obj)
	{
		AMGraph = new AdjMatrixGraph(*obj);
		graph = AMGraph;

	}

	Graph(AdjListGraph* obj)
	{
		ALGraph = new AdjListGraph(*obj);
		graph = ALGraph;
	}

	Graph(ListOfEdgesGraph* obj)
	{
		LEGraph = new ListOfEdgesGraph(*obj);
		graph = LEGraph;
	}

	Graph(char ind)
	{
		graph = new AdjMatrixGraph();
		CreateObjectGraph(ind);
	}

	Graph(char ind, int n, bool d, bool w)
	{
		graph = new AdjMatrixGraph();
		CreateObjectGraph(ind, n, 0, d, w);
	}

	~Graph()
	{
		delete graph;
	}

	void readGraph(std::string fileName)
	{
		std::ifstream fin(fileName, std::ios_base::in);
		char ind;
		fin >> ind;
		fin.close();

		//delete graph;

		CreateObjectGraph(ind);

		graph->readGraph(fileName);
	}

	void addEdge(int from, int to, int weight)
	{
		graph->addEdge(from, to, weight);
	}

	void removeEdge(int from, int to)
	{
		graph->removeEdge(from, to);
	}

	int changeEdge(int from, int to, int newWeight)
	{
		graph->changeEdge(from, to, newWeight);
	}

	void transformToAdjMatrix()
	{
		if (graph->IND != 'C')
		{
			if (graph->IND == 'L')
				AMGraph = new AdjMatrixGraph(*ALGraph);
			if (graph->IND == 'E')
				AMGraph = new AdjMatrixGraph(*LEGraph);
			delete graph;
			graph = AMGraph;
		}
	}

	void transformToAdjList()
	{
		if (graph->IND != 'L')
		{
			if (graph->IND == 'C')
				ALGraph = new AdjListGraph(*AMGraph);
			if (graph->IND == 'E')
				ALGraph = new AdjListGraph(*LEGraph);
			delete graph;
			graph = ALGraph;
		}
	}

	void transformToListOfEdges()
	{
		if (graph->IND != 'E')
		{
			if (graph->IND == 'C')
				LEGraph = new ListOfEdgesGraph(*AMGraph);
			if (graph->IND == 'L')
				LEGraph = new ListOfEdgesGraph(*ALGraph);
			delete graph;
			graph = LEGraph;
		}
	}

	void writeGraph(std::string fileName)
	{
		graph->writeGraph(fileName);
	}

	Graph getSpaingTreePrima()
	{
		if (graph->IND == 'C')
			return Graph(AMGraph->getSpaingTreePrima());
		if (graph->IND == 'L')
			return Graph(ALGraph->getSpaingTreePrima());
		if (graph->IND == 'E')
		{
			transformToAdjList();
			Graph a(ALGraph->getSpaingTreePrima());
			a.transformToListOfEdges();
			transformToListOfEdges();
			return Graph(a.LEGraph);
		}
	}

	Graph getSpaingTreeKruscal()
	{
		if (graph->IND == 'C')
		{
			transformToListOfEdges();
			Graph a(LEGraph->getSpaingTreeKruscal());
			a.transformToAdjMatrix();
			transformToAdjMatrix();
			return Graph(a.AMGraph);
		}
		if (graph->IND == 'L')
		{
			transformToListOfEdges();
			Graph a(LEGraph->getSpaingTreeKruscal());
			a.transformToAdjList();
			transformToAdjList();
			return Graph(a.ALGraph);
		}
		if (graph->IND == 'E')
			return Graph(LEGraph->getSpaingTreeKruscal());
	}

	Graph getSpaingTreeBoruvka()
	{
		if (graph->IND == 'C')
		{
			transformToListOfEdges();
			Graph a(LEGraph->getSpaingTreeBoruvka());
			a.transformToAdjMatrix();
			transformToAdjMatrix();
			return Graph(a.AMGraph);
		}
		if (graph->IND == 'L')
		{
			transformToListOfEdges();
			Graph a(LEGraph->getSpaingTreeBoruvka());
			a.transformToAdjList();
			transformToAdjList();
			return Graph(a.ALGraph);
		}
		if (graph->IND == 'E')
			return Graph(LEGraph->getSpaingTreeBoruvka());
	}

	int checkBipart(std::vector<char> &marks)
	{
		if (graph->IND == 'C')
			return AMGraph->checkBipart(marks);
		if (graph->IND == 'L')
		{
			AMGraph = new AdjMatrixGraph(*ALGraph);
			int res = AMGraph->checkBipart(marks);
			delete AMGraph;
			return res;
		}
		if (graph->IND == 'E')
		{
			AMGraph = new AdjMatrixGraph(*LEGraph);
			int res = AMGraph->checkBipart(marks);
			delete AMGraph;
			return res;
		}
	}

	std::vector<std::pair<int,int> > getMaximumMatchingBipart()
	{
		if (graph->IND == 'C')
			return AMGraph->getMaximumMatchingBipart();
		if (graph->IND == 'L')
		{
			AMGraph = new AdjMatrixGraph(*ALGraph);
			//delete ALGraph;
			std::vector<std::pair<int,int> > res = AMGraph->getMaximumMatchingBipart();
			//ALGraph = new AdjListGraph(*AMGraph);
			delete AMGraph;
			return res;
		}
		if (graph->IND == 'E')
		{
			AMGraph = new AdjMatrixGraph(*LEGraph);
			//delete LEGraph;
			std::vector<std::pair<int,int> > res = AMGraph->getMaximumMatchingBipart();
			//LEGraph = new ListOfEdgesGraph(*AMGraph);
			delete AMGraph;
			return res;
		}
	}
};